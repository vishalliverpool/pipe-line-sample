from flask import Blueprint

blueprint = Blueprint("hello_world", __name__, url_prefix="/hello_world")


@blueprint.route("/", methods=["GET", "POST"])
def hello_world():
    return "Hello World! from MessejApp team"
