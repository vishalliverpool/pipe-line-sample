from flask import Flask, Blueprint
from .route import blueprints


def create_app():
    app = Flask(__name__)
    for blueprint in blueprints:
        if isinstance(blueprint, Blueprint):
            app.register_blueprint(blueprint)

    return app
